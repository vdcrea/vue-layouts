const webpack = require('./build/webpack.styleguidist.conf')
const path = require('path')

module.exports = {
  title: 'Vue Layouts',
  showUsage: true,
  editorConfig: {
    theme: 'material',
  },
  styleguideDir: 'public',
  renderRootJsx: path.join(__dirname, './styleguide.root.js'),
  getComponentPathLine(componentPath) {
      const component = path.basename(componentPath, '.vue').split('.')[0]
      const dir = path.dirname(componentPath)
      return `import { ${component} } from 'vue-layouts'`
  },
  sections: [
    {
      name: 'Vue Layouts',
      content: 'src/readme.md'
    },
    {
      name: 'Layouts',
      components: 'src/components/**/*.vue'
    }
  ],
  webpackConfig: webpack
}
