/**
 * @mixin
 */
export default {
  props: {
    /**
     * Mobile first: apply to all screen sizes
     */
    xs: {
      type: [String, Number]
    },
    /**
     * Apply to screens starting at 768px wide
     */
    sm: {
      type: [String, Number]
    },
    /**
     * Apply to screens starting at 1024px wide
     */
    md: {
      type: [String, Number]
    },
    /**
     * Apply to screens starting at 1200px wide
     */
    lg: {
      type: [String, Number]
    }
  },
  watch: {
    xs () {
      this.setCss()
    },
    sm () {
      this.setCss()
    },
    md () {
      this.setCss()
    },
    lg () {
      this.setCss()
    }
  },
  data () {
    return {
      cssClasses: ''
    }
  },
  mounted () {
    this.setCss()
  }
}
