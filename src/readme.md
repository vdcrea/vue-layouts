Repo: https://gitlab.com/vdcrea/vue-layouts

Vue layouts is a set of components to create css grid layouts and flexbox rows.

Install
```html
npm i -S gitlab:vdcrea/vue-layouts
```

Install all the components:
```html
import Vue from 'vue'
import VueLayouts from 'vue-layouts'
import 'vue-layouts/dist/vue-layouts.css'

Vue.use(VueLayouts)
```

Use specific components:
```html
import Vue from 'vue'
import { CssGrid } from 'vue-layouts'
import 'vue-layouts/dist/vue-layouts.css'

Vue.component('CssGrid', CssGrid)
```
