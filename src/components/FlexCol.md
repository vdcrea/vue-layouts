Component using http://flexboxgrid.com/, child of `<FlexRow />` component.

For columns, possible values are for props `xs`, `sm`, `md` and `lg` are:
- width: n (1 to 12)
- offset: offset-n (1 to 12)

Example of autosized columns:
```js
<FlexRow>
  <FlexCol>
    <div class="grey-box">
      Column
    </div>
  </FlexCol>
  <FlexCol>
    <div class="grey-box">
      Column
    </div>
  </FlexCol>
</FlexRow>
```

Example of size and offset columns:
```js
<FlexRow>
  <FlexCol xs="5">
    <div class="grey-box">
      &lt;FlexCol xs="5" /&gt;
    </div>
  </FlexCol>
  <FlexCol xs="5 offset-2">
    <div class="grey-box">
      &lt;FlexCol xs="5 offset-2" /&gt;
    </div>
  </FlexCol>
</FlexRow>
```

Example of special positions:
```js
<FlexRow>
  <FlexCol>
    <div class="grey-box">
      Column 1
    </div>
  </FlexCol>
  <FlexCol xs="last">
    <div class="grey-box">
      Column 2
    </div>
  </FlexCol>
  <FlexCol xs="first">
    <div class="grey-box">
      Column 3
    </div>
  </FlexCol>
</FlexRow>
```
