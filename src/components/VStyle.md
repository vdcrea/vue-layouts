This component mount a `<style/>` tag inside components, Useful to add dynamic css based on data() or with media queries. It is used by `<CssGrid />` to inject inline styles.

Example of VStyle component
```
<div class="red">
  Red color from &lt;style/&gt; tag
  <VStyle>
  .red { color:red; font-family:monospace }
  </VStyle>
</div>
```
