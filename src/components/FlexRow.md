Component using http://flexboxgrid.com/, wrapper for `<FlexCol />` components.

Example of FlexRow with gutters, `<FlexRow gutters>`:
```js
<FlexRow gutters>
  <FlexCol>
    <div class="grey-box">
      Column
    </div>
  </FlexCol>
  <FlexCol>
    <div class="grey-box">
      Column
    </div>
  </FlexCol>
</FlexRow>
```

Example of horizontal alignment (start|center|end), `<FlexRow xs="center">`:
```js
<FlexRow xs="center">
  <FlexCol xs="3">
    <div class="grey-box">
      Column
    </div>
  </FlexCol>
</FlexRow>
```

Example of vertical alignment (top|middle|bottom), `<FlexRow xs="middle">`:
```js
<FlexRow xs="middle">
  <FlexCol xs="3">
    <div class="grey-box">
      Column
    </div>
  </FlexCol>
  <FlexCol xs="3">
    <div class="grey-box" style="line-height:100px">
      Column
    </div>
  </FlexCol>
</FlexRow>
```

Example of distribution (around|between), `<FlexRow xs="around">`:
```js
<FlexRow xs="around">
  <FlexCol xs="3">
    <div class="grey-box">
      Column
    </div>
  </FlexCol>
  <FlexCol xs="3">
    <div class="grey-box">
      Column
    </div>
  </FlexCol>
</FlexRow>
```

Example of reverse order, `<FlexRow reverse>`:
```js
<FlexRow reverse>
  <FlexCol xs="3">
    <div class="grey-box">
      Column 1
    </div>
  </FlexCol>
  <FlexCol xs="3">
    <div class="grey-box">
      Column 2
    </div>
  </FlexCol>
  <FlexCol xs="3">
    <div class="grey-box">
      Column 3
    </div>
  </FlexCol>
  <FlexCol xs="3">
    <div class="grey-box">
      Column 4
    </div>
  </FlexCol>
</FlexRow>
```
