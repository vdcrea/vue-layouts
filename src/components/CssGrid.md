`xs`, `sm`, `md` and `lg` must be grid templates with line breaks and all sizes, example:

```html
'header  header' 120px
'content column' auto
'content news'   150px
'content news'   auto
'footer  footer' auto /
1fr      2fr
```

All grid areas (here: header, content, column, news, footer) become named `<slots>` available for you to populate with your content. Example: use `<slots name="header">Header</slot>` within the `<CssGrid>` component to populate your header content.

`xs` is a required prop while `sm`, `md` and `lg` are applied for breakpoints defined in `bp`. Default value of `bp` is:

```html
[
  {s: 'sm', w: 768},
  {s: 'md', w: 1024},
  {s: 'xl', w: 1200}
]
```
Order matters, breakpoints in `bp` array must be ordered from the smallest to the largest screen size. Breakpoints width property `w` is set with Numbers (pixels), you can also use Strings with units ex: `{s: 'sm', w: '48em'}`.

If your breakpoints are defined with Numbers, you can have different grid areas depending on the breakpoint. If breakpoints are Strings all your grid areas must be the same across all your breakpoints.

Example of CssGrid with different areas depending on screen width:

```
<CssGrid
  :style="{ height: '75vh' }"
  xs-gap="20px"
  xs="  'header' 80px
        'content' auto
        'column' auto
        'footer' auto /
        100%"
  sm-gap="20px"
  sm="  'header header' 80px
        'column news' auto
        'column content' auto
        'footer footer' 40px /
        1fr 2fr">

  <div slot="header">
    <div class="grey-cell">
      Header
    </div>
  </div>
  <div slot="content">
    <div class="grey-cell">
      Content
    </div>
  </div>
  <div slot="column">
    <div class="grey-cell">
      Column
    </div>
  </div>
  <div slot="news">
    <div class="grey-cell">
      News
    </div>
  </div>
  <div slot="footer">
    <div class="grey-cell">
      Footer
    </div>
  </div>

</CssGrid>
```
