import kebabCase from 'lodash/kebabCase'
import intersection from 'lodash/intersection'

/*
 Parse css grid template string

 ex:
  const grid = "  'header header' 120px
       'content column' auto
       'content news' 150px
       'content news' auto
       'footer footer' auto /
       1fr 2fr"

  console.log(parseGrid(grid))

 return :
  {
    structure: [
      ["header", "header"],
      ["content", "column"],
      ["content", "news"],
      ["content", "news"],
      ["footer", "footer"]
    ],
    areas: ["header", "content", "column", "news", "footer"],
    rows: "120px auto 150px auto auto",
    cols: "1fr 2fr"
  }
 */
export const parseGrid = (grid) => {
  return grid.replace('/', '').split('\n')
    .reduce((grid, line, i, a) => {
      let l = line.trim()
      if (!grid.structure) grid.structure = []
      const as = l.match(/'([^']+)'/)
      if (as && as[1]) {
        if (!grid.areas) grid.areas = []
        const lineAreas = as[1].split(' ')
          .filter(las => las)
          .map(a => {
            const clean = a.trim()
            if (!grid.areas.includes(a)) grid.areas.push(clean)
            return clean
          })
        grid.structure.push(lineAreas)
        l = l.replace(`'${as[1]}'`, ' ').trim()
        if (!grid.rows) grid.rows = [l]
        else grid.rows.push(l)
      } else if (l.length) {
        grid.cols = l.split(' ').filter(l => l).join(' ')
      }
      if (i === a.length - 1) {
        if (grid.rows) grid.rows = grid.rows.join(' ')
      }
      return grid
    }, {})
}

/*
 Accept a grid.structure from the function parseGrid()
 and returns a grid structure with row in columns injected only if gap is defined

 ex:
  const structure = [
    ["header", "header"],
    ["content", "column"],
    ["content", "news"],
    ["content", "news"],
    ["footer", "footer"]
  ]
  console.log(msStructure(structure, '15px'))

 returns:
  [
    ["header", "header", "header"],
    ["", "", ""],
    ["content", "", "column"],
    ["content", "", ""],
    ["content", "", "news"],
    ["content", "", "news"],
    ["content", "", "news"],
    ["", "", ""],
    ["footer", "footer", "footer"]
  ]

*/
const msGridStructure = (structure, gap) => {
  return gap ? structure.map(l => l.reduce((line, area, i, l) => {
    line.push(area)
    if (l[i + 1] && l[i + 1] === area) line.push(area)
    else if (l[i + 1]) line.push('')
    return line
  }, [])).reduce((shape, line, i, s) => {
    shape.push(line)
    if (s[i + 1]) {
      const intersect = intersection(line, s[i + 1])
      const marginLine = line.map(a => {
        if (intersect.includes(a)) return a
        else return ''
      })
      shape.push(marginLine)
    }
    return shape
  }, []) : structure
}

/*
 Accept an Array of areas from parseGrid()
 and an Array of msStructure from msGridStructure()
 and returns an Object defining IE css props

 ex:
  const areas = ['header', 'content', 'column', 'news', 'footer']
  const msStructure = [
    ["header", "header", "header"],
    ["", "", ""],
    ["content", "", "column"],
    ["content", "", ""],
    ["content", "", "news"],
    ["content", "", "news"],
    ["content", "", "news"],
    ["", "", ""],
    ["footer", "footer", "footer"]
  ]

  console.log(msAreas(areas, msStructure))

 returns:
  [
    {
      area: "header",
      row: 1,
      column: 1,
      rowSpan: 1,
      columnSpan: 3
    },
    {
      area: "content",
      row: 3,
      column: 1,
      rowSpan: 5,
      columnSpan: 1
    },
    {
      area: "column",
      row: 3,
      column: 3,
      rowSpan: 1,
      columnSpan: 1
    },
    {
      area: "news",
      row: 5,
      column: 3,
      rowSpan: 3,
      columnSpan: 1
    },
    {
      area: "footer",
      row: 9,
      column: 1,
      rowSpan: 1,
      columnSpan: 3
    }
  ]
 */
const msAreas = (areas, msStructure) => {
  return areas.map(area => {
    let row = null
    let column = null
    let rowSpan = 0
    let columnSpan = 0
    for (let y = 0; y < msStructure.length; y++) {
      const line = msStructure[y]
      if (row === null && line.includes(area)) row = y
      else if (line.includes(area)) rowSpan = y - row
      for (let x = 0; x < line.length; x++) {
        if (column === null && line[x] === area) column = x
        else if (line[x] === area) columnSpan = x - column
      }
      // increment to move from js to css units
      if (y === msStructure.length - 1) {
        row++
        column++
        rowSpan++
        columnSpan++
      }
    }
    return { area, row, column, rowSpan, columnSpan }
  })
}

/*
 Utility
 Accept a String from parseGrid() rows or cols
 and returns the converted String with gap injection
 for css value of -ms-grid-columns and -ms-grid-rows

 ex:
  const size = '120px auto 150px auto auto'
  const gap = '20px'

  console.log(msSize(size, gap))

 returns:
  '120px 20px auto 20px 150px 20px auto 20px auto'
 */

const msSize = (size, gap) => {
  const insertString = (string, target, needle) => string.split(target).join(needle)
  return gap ? insertString(size, ' ', ` ${gap} `) : size
}

/*
 Utility
 Accept a String prop and a value (String or Number)
 and returns the css String prefixed

 ex:
  console.log(msCssProp('rows', 1))

 returns:
  -ms-grid-rows:1;
 */
const msCssProp = (prop, val) => val.length ? `-ms-grid-${prop}:${val};` : ''

/*
 Accept a String id, and an Object msAreas from msAreas()
 and returns the css String for all grid areas

 ex:
  const id = 'test'
  msAreas =

  console.log(msAreasToCss(id, msAreas))

 returns:
  "#test .header{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:3;grid-area:header;},#test .content{-ms-grid-row:3;-ms-grid-column:1;-ms-grid-row-span:5;grid-area:content;},#test .column{-ms-grid-row:3;-ms-grid-column:3;grid-area:column;},#test .news{-ms-grid-row:5;-ms-grid-column:3;-ms-grid-row-span:3;grid-area:news;},#test .footer{-ms-grid-row:9;-ms-grid-column:1;-ms-grid-column-span:3;grid-area:footer;}"
 */

const msAreasToCss = (id, msAreas) => {
  const noOne = ['row-span', 'column-span']
  return msAreas.map(a => {
    let cssProps = Object.keys(a)
      .filter(k => k !== 'area')
      .reduce((css, prop) => {
        const val = a[prop]
        const cssProp = kebabCase(prop)
        if (noOne.includes(cssProp) && val > 1) {
          css += msCssProp(cssProp, val.toString())
        } else if (!noOne.includes(cssProp)) {
          css += msCssProp(cssProp, val.toString())
        }
        return css
      }, '')
    cssProps += `grid-area:${a.area};`
    return `#${id} .cell-${a.area}{${cssProps}}`
  }).join(' ')
}

const msGridToCss = (id, grid, gap, template) => {
  const areas = grid.structure.map(l => {
    return '"' + l.join(' ') + '"'
  }).join(' ')
  const cssGap = gap ? `grid-gap:${gap};` : ''
  const msRows = msCssProp('rows', msSize(grid.rows, gap))
  const msCols = msCssProp('columns', msSize(grid.cols, gap))
  return `#${id}{display:-ms-grid;display:grid;${cssGap}${msRows}${msCols}grid-template-columns:${grid.cols};grid-template-rows:${grid.rows};grid-template-areas:${areas};}`
}

// const excludedAreasToCss = (id, areas) => {
//   return areas ? areas.map(a => `#${id} .cell-${a}{display:none}`).join() : ''
// }

export const gridToCss = (id, templates) => { // (id, slots, templates) => {
  return templates.map(t => {
    const parsedGrid = parseGrid(t.tpl)
    const msStructure = msGridStructure(parsedGrid.structure, t.gap)
    const areas = msAreas(parsedGrid.areas, msStructure)
    // const excludedAreas = slots.reduce((excl, area) => {
    //   if (!parsedGrid.areas.includes(area)) excl.push(area)
    //   return excl
    // }, [])
    let cssString = msGridToCss(id, parsedGrid, t.gap, t.tpl)
    cssString += msAreasToCss(id, areas)
    // cssString += excludedAreasToCss(id, excludedAreas)
    if (t.w) {
      const bpVal = isNaN(t.w) ? t.w : t.w + 'px'
      cssString = `@media(min-width:${bpVal}){${cssString}}`
    }
    return cssString
  }).join('\n')
}
