import CssGrid from './components/CssGrid.vue'
import FlexCol from './components/FlexCol.vue'
import FlexRow from './components/FlexRow.vue'
import VStyle from './components/VStyle.vue'

// Install the components
export function install (Vue) {
  Vue.component('CssGrid', CssGrid)
  Vue.component('FlexCol', FlexCol)
  Vue.component('FlexRow', FlexRow)
  Vue.component('VStyle', VStyle)
  /* -- Add more components here -- */
}

// Expose the components
export {
  CssGrid,
  FlexCol,
  FlexRow,
  VStyle
  /* -- Add more components here -- */
}

/* -- Plugin definition & Auto-install -- */
/* You shouldn't have to modify the code below */

// Plugin
const plugin = {
  /* eslint-disable no-undef */
  version: VERSION,
  install
}

export default plugin

// Auto-install
let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(plugin)
}
